FROM centos

RUN yum -y install java

RUN yum install wget -y

RUN yum install git -y

RUN wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo

RUN rpm --import https://pkg.jenkins.io/redhat/jenkins.io.key

RUN yum install jenkins -y

WORKDIR /opt

RUN wget https://download.java.net/java/GA/jdk13/5b8a42f3905b406298b72d750b6919f6/33/GPL/openjdk-13_linux-x64_bin.tar.gz

RUN tar -xvf openjdk-13_linux-x64_bin.tar.gz

RUN echo "export JAVA_HOME=/opt/jdk-13/" >> /root/.bashrc

RUN echo "export PATH=$PATH:/opt/jdk-13/bin" >> /root/.bashrc

RUN source  /root/.bashrc

RUN echo $JAVA_HOME

RUN java -version

WORKDIR /usr/local/src

RUN wget http://www-us.apache.org/dist/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz

RUN tar -xf apache-maven-3.5.4-bin.tar.gz

RUN mv apache-maven-3.5.4/ apache-maven/

RUN echo "export MAVEN_HOME=/usr/local/src/apache-maven" >> /root/.bashrc

RUN echo "export PATH=${MAVEN_HOME}/bin:${PATH}" >> /root/.bashrc

RUN  source /root/.bashrc

WORKDIR /root/.jenkins/workspace/

CMD ["java", "-jar", "/usr/lib/jenkins/jenkins.war"]






